package ec.edu.ups.appdis.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Arrendatario.class)
public abstract class Arrendatario_ {

	public static volatile SingularAttribute<Arrendatario, String> clave;
	public static volatile SingularAttribute<Arrendatario, String> cedula;
	public static volatile SingularAttribute<Arrendatario, String> apellido;
	public static volatile SingularAttribute<Arrendatario, String> usuario;
	public static volatile ListAttribute<Arrendatario, Predio> predios;
	public static volatile SingularAttribute<Arrendatario, String> nombre;
	public static volatile SingularAttribute<Arrendatario, String> email;

}

