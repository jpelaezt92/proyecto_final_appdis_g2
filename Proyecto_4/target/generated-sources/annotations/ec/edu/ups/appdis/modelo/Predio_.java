package ec.edu.ups.appdis.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Predio.class)
public abstract class Predio_ {

	public static volatile SingularAttribute<Predio, String> codigo;
	public static volatile ListAttribute<Predio, Imagen> imagenes;
	public static volatile SingularAttribute<Predio, Arrendatario> arrendatario;
	public static volatile SingularAttribute<Predio, String> direccion;
	public static volatile SingularAttribute<Predio, Double> x;
	public static volatile SingularAttribute<Predio, Double> y;

}

