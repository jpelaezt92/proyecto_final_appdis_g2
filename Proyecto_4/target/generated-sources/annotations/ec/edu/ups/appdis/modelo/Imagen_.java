package ec.edu.ups.appdis.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Imagen.class)
public abstract class Imagen_ {

	public static volatile SingularAttribute<Imagen, String> codigo;
	public static volatile SingularAttribute<Imagen, Predio> predio;
	public static volatile SingularAttribute<Imagen, String> direccion;
	public static volatile SingularAttribute<Imagen, String> nombre;

}

