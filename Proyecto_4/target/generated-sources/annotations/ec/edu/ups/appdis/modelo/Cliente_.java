package ec.edu.ups.appdis.modelo;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Cliente.class)
public abstract class Cliente_ {

	public static volatile SingularAttribute<Cliente, String> clave;
	public static volatile SingularAttribute<Cliente, String> cedula;
	public static volatile SingularAttribute<Cliente, String> apellido;
	public static volatile SingularAttribute<Cliente, String> usuario;
	public static volatile SingularAttribute<Cliente, String> nombre;
	public static volatile SingularAttribute<Cliente, String> email;

}

