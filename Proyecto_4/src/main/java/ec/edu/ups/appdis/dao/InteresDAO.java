package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.modelo.Arrendatario;
import ec.edu.ups.appdis.modelo.Interes;
import ec.edu.ups.appdis.modelo.Predio;


@Stateless
public class InteresDAO {
	
	@Inject
	private EntityManager em;

	public void insert(Interes interes) {
		em.persist(interes);
	}
	
	public List<Interes> getContarVistosPredios(int codigo) {
		String jpql = "SELECT int FROM Interes int WHERE int.valor = :variable1";
		Query query = em.createQuery(jpql, Interes.class).setParameter("variable1", codigo);
		List<Interes> lista = query.getResultList();
		return lista;
	}
	
	/*public List<Interes> getContarVistosPredios() {
		String jpql = "SELECT int FROM Interes int";
		Query query = em.createQuery(jpql, Interes.class);
		List<Interes> lista = query.getResultList();
		return lista;
	}
	
	public int contador_cliente() {
		int aux = 0;
		try {
			 aux= (Integer) em.createNativeQuery("select max(cli_codigo) from cliente").getSingleResult();
			 return aux;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return aux;
	}
	
	public Double getContarVistosPredios(int codigo) {
		try {
			System.out.println(codigo+" ccccccccccccccccccccccccccc");
			String jpql = "SELECT COUNT(int.valor) FROM Interes int WHERE int.valor =:variable1";
			Double query = (Double) em.createQuery(jpql).setParameter("variable1", codigo).getSingleResult();
			return query;
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}*/
}
