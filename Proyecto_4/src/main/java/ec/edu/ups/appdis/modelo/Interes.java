package ec.edu.ups.appdis.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_interes_prueba")
public class Interes {
	
	@Id
	@Column(name = "int_codigo")
	@GeneratedValue
	private int codigo;
	
	@Column(name = "int_valor")
	private int valor;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}
}
