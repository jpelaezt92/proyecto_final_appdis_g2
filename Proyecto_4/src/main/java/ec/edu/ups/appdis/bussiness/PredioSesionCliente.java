package ec.edu.ups.appdis.bussiness;


import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ec.edu.ups.appdis.modelo.Arrendatario;
import ec.edu.ups.appdis.modelo.Predio;

@Named 
@SessionScoped
public class PredioSesionCliente implements Serializable{
	
	private String codigo;
	private String direccion;
	private String x;
	private String y;
	
	private Predio predio;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getX() {
		return x;
	}

	public void setX(String x) {
		this.x = x;
	}

	public String getY() {
		return y;
	}

	public void setY(String y) {
		this.y = y;
	}

	public Predio getPredio() {
		return predio;
	}

	public void setPredio(Predio predio) {
		this.predio = predio;
	}
}
