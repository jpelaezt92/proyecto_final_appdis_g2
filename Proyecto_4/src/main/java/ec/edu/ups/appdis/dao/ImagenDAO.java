package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.modelo.Imagen;
import ec.edu.ups.appdis.modelo.Predio;

@Stateless
public class ImagenDAO {
	
	@Inject
	private EntityManager em;

	public void insert(Imagen imagen) {
		em.persist(imagen);
	}
	
	public void update(Imagen imagen) {
		em.merge(imagen);
	}
	
	public void remove(String id) {
		em.remove(this.read(id));
	}
	
	public Imagen read(String id) {
		Imagen imagen = em.find(Imagen.class, id);
		return imagen;
	}
	
	public List<Imagen> getImagens(){
		String jpql = "SELECT ima FROM Imagen ima";
		Query query = em.createQuery(jpql, Imagen.class);
		List<Imagen> listado = query.getResultList();		
		return listado;
	}
	
	public List<Imagen> getImagenesxPredio(String codigo) {
		String jpql = "SELECT ima FROM Imagen ima JOIN FETCH ima.predio WHERE ima.predio.codigo = :variable1";
		Query query = em.createQuery(jpql, Imagen.class).setParameter("variable1", codigo);
		List<Imagen> lista = query.getResultList();
		return lista;
	}
}
