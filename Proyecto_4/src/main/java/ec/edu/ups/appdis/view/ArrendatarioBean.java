package ec.edu.ups.appdis.view;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import ec.edu.ups.appdis.bussiness.ArrendatarioBussiness;
import ec.edu.ups.appdis.bussiness.ArrendatarioSesion;
import ec.edu.ups.appdis.dao.ArrendatarioDAO;
import ec.edu.ups.appdis.modelo.Arrendatario;

@ManagedBean
@ViewScoped
public class ArrendatarioBean {
	
	@Inject
	private ArrendatarioBussiness arrBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private ArrendatarioDAO arrDao;
	
	@Inject
	private ArrendatarioSesion arrSesion;
	
	private Arrendatario newArrendatario;
	
	private String id;  		//Parametro para edicion
	
	private List<Arrendatario> arrendatarios;
	
	private List<Arrendatario> arrendatariosYpredios;
	
	private boolean editing;
	
	@PostConstruct
	public void init() {
		newArrendatario = new Arrendatario();
		//newArrendatario.addTelefono(new Telefono());
		editing = false;
		arrendatarios = arrBussiness.getListadoArrendatario();
	}	

	
	public void loadData() {
		System.out.println("load data " + id);
		if(id==null)
			return;
		try {
			newArrendatario = arrBussiness.getArrendatario(id);
			editing = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}
	}
	public Arrendatario getNewArrendatario() {
		return newArrendatario;
	}
	public void setNewArrendatario(Arrendatario newArrendatario) {
		this.newArrendatario = newArrendatario;
	}
	public List<Arrendatario> getArrendatarios() {
		return arrendatarios;
	}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		System.out.println("id param " + id);
		this.id = id;
	}


	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public void setArrendatarios(List<Arrendatario> arrendatarios) {
		this.arrendatarios = arrendatarios;
	}

	public ArrendatarioSesion getArrSesion() {
		return arrSesion;
	}


	public void setArrSesion(ArrendatarioSesion arrSesion) {
		this.arrSesion = arrSesion;
	}

	public List<Arrendatario> getArrendatariosYpredios() {
		return arrendatariosYpredios;
	}

	public void setArrendatariosYpredios(List<Arrendatario> arrendatariosYpredios) {
		this.arrendatariosYpredios = arrendatariosYpredios;
	}

	public String guardar() {
		System.out.println("editando " + editing);
		try {
			if(editing)
				arrBussiness.actualizar(newArrendatario);
			else
				arrBussiness.save(newArrendatario);
			System.out.println("Registro guardado");
			return "loginArrendatario?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String eliminar(String cedula) {
		
		try {
			arrBussiness.eliminar(cedula);
			System.out.println("Registro eliminado");
			return "list-arrendatarios?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al eliminar");
			e.printStackTrace();			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String editar(Arrendatario arrendatario) {
		editing = true;
		System.out.println(arrendatario);
		//newPersona = persona;
		return "edit-arrendatario?faces-redirect=true&id="+arrendatario.getCedula();
	}
	
	public String login() {
		newArrendatario = arrDao.loginArrendatario(newArrendatario.getUsuario(), newArrendatario.getClave());
		if (newArrendatario == null) {
			newArrendatario = new Arrendatario();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario o contrasena incorrectos!", " Login Error!")); 
			return null;
		} else {
			Arrendatario arrendatario = new Arrendatario();
			arrSesion.setUsuario(newArrendatario.getUsuario());
			arrSesion.setEmail(newArrendatario.getEmail());
			arrSesion.setCedula(newArrendatario.getCedula());
			arrendatario.setCedula(newArrendatario.getCedula());
			arrendatario.setNombre(newArrendatario.getNombre());
			arrendatario.setApellido(newArrendatario.getApellido());
			arrendatario.setEmail(newArrendatario.getEmail());
			arrendatario.setUsuario(newArrendatario.getUsuario());
			arrendatario.setClave(newArrendatario.getClave());
			arrSesion.setArrendatario(arrendatario);
			return "arrendatario-principal?faces-redirect=true";
		}
	}
	
	public void logout() {
		ExternalContext ec=FacesContext.getCurrentInstance().getExternalContext();
		ec.invalidateSession();
		//return "/home.xhtml?faces-redirect=true";\
        try {
			ec.redirect(ec.getRequestContextPath() + "/loginArrendatario.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Arrendatario> arrendatario(String usuario) {
		List<Arrendatario> a = arrDao.getArrendatarioUnico2(usuario);
		return a;
	}
	
	public void validadorCedula(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String cedula = (String)arg2;
		String aux = arrDao.cedula(cedula);
		System.out.println(aux);
		if (aux.equals("no")) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("Cedula invalida"));
		}
	}
	
	public void validadorUsuario(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String usuario = (String)arg2;
		Arrendatario aux = arrDao.getUsuario(usuario); 
		System.out.println(aux);
		if (aux != null) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("El usuario ya existe"));
		}
	}
	
	public void validadorCorreo(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String correo = (String)arg2;
		Arrendatario aux = arrDao.getCorreo(correo); 
		System.out.println(aux);
		if (aux != null) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("El correo ya esta en uso"));
		}
	}
	
	public String regresar() {
		return "loginArrendatario.xhtml";
	}
}
