package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.modelo.Cliente;

@Stateless
public class ClienteBussiness {

	@Inject
	private ClienteDAO cliDAO;
	
	
	public boolean save(Cliente cliente) throws Exception {
		Cliente aux = cliDAO.read(cliente.getCedula());
		if(aux!=null)
			throw new Exception("Cliente ya existe");
		else
			cliDAO.insert(cliente);	
		
		return true;
	}
	
	public List<Cliente> getListadoCliente(){
		return cliDAO.getClientes();
	}
	
	public void eliminar(String cedula) throws Exception {
		Cliente aux = cliDAO.read(cedula);
		if(aux==null)
			throw new Exception("Cliente no existe");
		else
			cliDAO.remove(cedula);
	}
	
	public Cliente getCliente(String cedula) throws Exception {
		Cliente aux = cliDAO.read(cedula);
		if(aux==null)
			throw new Exception("Cliente no existe");
		else
			return aux;
	}
	
	public void actualizar(Cliente cliente) throws Exception {
		Cliente aux = cliDAO.read(cliente.getCedula());
		if(aux==null)
			throw new Exception("Cliente no existe");
		else
			cliDAO.update(cliente);
	}
	
	public String guardar(Cliente cliente) throws Exception {
				
			cliDAO.insert(cliente);	
		
		return "registrado";
	}
}
