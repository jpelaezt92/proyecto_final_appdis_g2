package ec.edu.ups.appdis.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import ec.edu.ups.appdis.modelo.Arrendatario;




@Stateless
public class ArrendatarioDAO {
	
	@Inject
	private EntityManager em;

	public void insert(Arrendatario arrendatario) {
		em.persist(arrendatario);
	}
	
	public void update(Arrendatario arrendatario) {
		em.merge(arrendatario);
	}
	
	public void remove(String id) {
		em.remove(this.read(id));
	}
	
	public Arrendatario read(String id) {
		Arrendatario arrendatario = em.find(Arrendatario.class, id);
		return arrendatario;
	}
	
	public List<Arrendatario> getArrendatarios(){
		String jpql = "SELECT arr FROM Arrendatario arr";
		Query query = em.createQuery(jpql, Arrendatario.class);
		List<Arrendatario> listado = query.getResultList();		
		return listado;
	}
	
	public Arrendatario loginArrendatario(String usuario, String clave) {
		try {
			String jpql = "SELECT arr FROM Arrendatario arr WHERE arr.usuario = :variable1 and arr.clave = :variable2";
			Arrendatario query = (Arrendatario) em.createQuery(jpql).setParameter("variable1", usuario) .setParameter("variable2", clave).getSingleResult();
			return query;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public List<Arrendatario> getArrendatarioUnico2(String usuario) {
		String jpql = "SELECT arr FROM Arrendatario arr WHERE arr.usuario = :variable1";
		Query query = em.createQuery(jpql, Arrendatario.class).setParameter("variable1", usuario);
		List<Arrendatario> lista = query.getResultList();
		return lista;
	}
	
	public String cedula(String cedula) {
		String hola = "no";
		try {
			ArrayList<String> cedula1 = new ArrayList<String>();
			ArrayList<Integer> cedula2 = new ArrayList<Integer>();
			for (int i = 1; i <= cedula.length(); i++) {
				cedula1.add(cedula.substring(i-1, i));
				//System.out.println(cedula1.get(i-1));
				cedula2.add(Integer.parseInt(cedula1.get(i-1)));
				//System.out.println(cedula2.get(i-1));
			}
			ArrayList<Integer> cedula3 = new ArrayList<Integer>();
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			ArrayList<Integer> cedula4 = new ArrayList<Integer>();
			ArrayList<Integer> cedula5 = new ArrayList<Integer>();
			for (int i = 0; i <= cedula3.size()-1; i++) {
				cedula4.add(cedula2.get(i)*cedula3.get(i));
				if (cedula4.get(i)>10) {
					int decena = cedula4.get(i)/10;
					int unidad = cedula4.get(i)%10;
					cedula5.add(decena+unidad);
					//System.out.println(cedula5.get(i));
				} else {
					cedula5.add(cedula4.get(i));
					//System.out.println(cedula5.get(i));
				}
			}
			int subtotal = cedula5.get(0)+cedula5.get(1)+cedula5.get(2)+cedula5.get(3)+cedula5.get(4)+cedula5.get(5)+cedula5.get(6)+cedula5.get(7)+cedula5.get(8);
			//System.out.println(subtotal);
			int aux = ((subtotal/10)+1)*10;
			//System.out.println(aux);
			int total = aux-subtotal;
			//System.out.println(total);
			int aux2 = subtotal%10;
			//System.out.println(aux2);
			if (aux2==cedula2.get(9)) {
				hola = "si";
			} else if (total==cedula2.get(9)) {
				hola = "si";
			} 
			

		} catch (Exception e) {
			// TODO: handle exception
			
		}
		return hola;
	}
	
	public Arrendatario getUsuario(String usuario){ 
		try {
			Arrendatario arrendatario = (Arrendatario) em .createQuery( "SELECT arr from Arrendatario arr where arr.usuario = :name ").setParameter("name", usuario).getSingleResult();
			System.out.println(arrendatario);
			return arrendatario; 
			} catch (NoResultException e) { 
				
				}
		return null; 
	}
	
	public Arrendatario getCorreo(String correo){ 
		try {
			Arrendatario arrendatario = (Arrendatario) em .createQuery( "SELECT arr from Arrendatario arr where arr.email = :name ").setParameter("name", correo).getSingleResult();
			System.out.println(arrendatario);
			return arrendatario; 
			} catch (NoResultException e) { 
				return null; 
				}
	}
}
