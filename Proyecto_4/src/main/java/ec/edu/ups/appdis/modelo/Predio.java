package ec.edu.ups.appdis.modelo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="tbl_predio_prueba")
public class Predio {
	
	@Id
	@Column(name = "pre_codigo")
	private String codigo;
	
	@Column(name = "pre_direccion")
	private String direccion;
	
	@Column(name = "pre_lonfitud")
	private Double x;
	
	@Column(name = "pre_altitud")
	private Double y;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pre_arr_cedula")
	private Arrendatario arrendatario;
	
	 @OneToMany(mappedBy = "predio", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	 @JsonBackReference
     private List<Imagen> imagenes;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}
	
	public Arrendatario getArrendatario() {
		return arrendatario;
	}

	public void setArrendatario(Arrendatario arrendatario) {
		this.arrendatario = arrendatario;
	}

	public List<Imagen> getImagenes() {
		return imagenes;
	}

	public void setImagenes(List<Imagen> imagenes) {
		this.imagenes = imagenes;
	}
}
