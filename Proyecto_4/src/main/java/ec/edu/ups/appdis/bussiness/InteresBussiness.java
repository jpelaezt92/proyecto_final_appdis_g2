package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.InteresDAO;
import ec.edu.ups.appdis.modelo.Interes;

@Stateless
public class InteresBussiness {

	@Inject
	private InteresDAO intDAO;
	
	
	public void save(Interes interes) throws Exception {
		intDAO.insert(interes);	
	}
	
	public List<Interes> getListadoInteresxPredio(int codigo){
		return intDAO.getContarVistosPredios(codigo);
	}
	/*
	public List<Interes> getListadoInteresxPredio(){
		return intDAO.getContarVistosPredios();
	}*/
}
