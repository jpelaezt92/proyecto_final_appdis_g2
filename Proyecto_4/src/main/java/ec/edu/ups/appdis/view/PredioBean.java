package ec.edu.ups.appdis.view;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

import ec.edu.ups.appdis.bussiness.ArrendatarioSesion;
import ec.edu.ups.appdis.bussiness.ArrendatarioSesionCliente;
import ec.edu.ups.appdis.bussiness.ClienteSesion;
import ec.edu.ups.appdis.bussiness.PredioBussiness;
import ec.edu.ups.appdis.bussiness.PredioSesion;
import ec.edu.ups.appdis.bussiness.PredioSesionCliente;
import ec.edu.ups.appdis.bussiness.PredioSesionImagen;
import ec.edu.ups.appdis.dao.ArrendatarioDAO;
import ec.edu.ups.appdis.dao.ImagenDAO;
import ec.edu.ups.appdis.dao.InteresDAO;
import ec.edu.ups.appdis.modelo.Imagen;
import ec.edu.ups.appdis.modelo.Interes;
import ec.edu.ups.appdis.modelo.Predio;


@ManagedBean
@ViewScoped
public class PredioBean {
	
	@Inject
	private PredioBussiness preBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private ArrendatarioSesion arrSesion;
	
	@Inject
	private PredioSesionCliente preSesionCliente;
	
	@Inject
	private PredioSesion preSesion;
	
	@Inject
	private PredioSesionImagen preImaSesion;
	
	@Inject
	private PredioSesionCliente preImaSesCli;
	
	@Inject
	private ArrendatarioSesionCliente arrSesCli;
	
	@Inject
	private ClienteSesion cliSesion;
	
	@Inject
	private InteresDAO intDao;
	
	@Inject
	private ImagenDAO imaDao;
	
	private Predio newPredio;
	
	private String id;  		//Parametro para edicion
	
	private List<Predio> predios;
	
	private List<Predio> prediosYarrendatarios;
	
	private boolean editing;
	
	private Predio nP;
	
	private List<Predio> nPi;
	
	private List<Predio> nPi2;
	
	private List<Predio> nPi3;
	
	private List<Predio> nPi4;
	
	private List<Predio> nPi5;
	
	@PostConstruct
	public void init() {
		newPredio = new Predio();
		//newPredio.addImagen(new Imagen());
		editing = false;
		predios = preBussiness.getListadoPredio();
		prediosYarrendatarios = preBussiness.getListadoPredioYarrendatario(arrSesion.getCedula());
		nPi = preBussiness.getListadoPredioYimagen(preImaSesion.getCodigo());
		nPi2 = nPi;
		nPi3 = preBussiness.getListadoPredioYimagen(preImaSesCli.getCodigo());
		nPi4 = nPi3;
	}	
	
	public void loadData() {
		System.out.println("load data " + id);
		if(id==null)
			return;
		try {
			newPredio = preBussiness.getPredio(id);
			editing = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}
	}
	public Predio getNewPredio() {
		return newPredio;
	}
	public void setNewPredio(Predio newPredio) {
		this.newPredio = newPredio;
	}
	public List<Predio> getPredios() {
		return predios;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		System.out.println("id param " + id);
		this.id = id;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public void setPredios(List<Predio> predios) {
		this.predios = predios;
	}

	public List<Predio> getPrediosYarrendatarios() {
		return prediosYarrendatarios;
	}

	public void setPrediosYarrendatarios(List<Predio> prediosYarrendatarios) {
		this.prediosYarrendatarios = prediosYarrendatarios;
	}

	public PredioSesionCliente getPreSesionCliente() {
		return preSesionCliente;
	}

	public void setPreSesionCliente(PredioSesionCliente preSesionCliente) {
		this.preSesionCliente = preSesionCliente;
	}

	public PredioSesionImagen getPreImaSesion() {
		return preImaSesion;
	}

	public void setPreImaSesion(PredioSesionImagen preImaSesion) {
		this.preImaSesion = preImaSesion;
	}

	public List<Predio> getnPi2() {
		return nPi2;
	}

	public void setnPi2(List<Predio> nPi2) {
		this.nPi2 = nPi2;
	}

	public List<Predio> getnPi4() {
		return nPi4;
	}

	public void setnPi4(List<Predio> nPi4) {
		this.nPi4 = nPi4;
	}

	public String guardar() {
		System.out.println("editando " + editing);
		int aux = predios.size();
		System.out.println(aux);
		String aux3;
		if (aux == 0) {
			System.out.println("siiiiiiiiiiiiiiiiiiiiiiii");
			aux3 = "1";
			System.out.println(aux3+" asdffffffffffffffffffffffffffffffff"); 
		} else {
			String aux1 = predios.get(aux-1).getCodigo();
			int aux2 = Integer.parseInt(aux1)+1;
			System.out.println(aux2);
			aux3 = String.valueOf(aux2);
			System.out.println(aux3+" asdffffffffffffffffffffffffffffffff");
		}
		try {
			if(editing)
				preBussiness.actualizar(newPredio);
			else
				
				nP = new Predio();
				nP.setCodigo(aux3);
				nP.setDireccion(newPredio.getDireccion());
				nP.setX(newPredio.getX());
				nP.setY(newPredio.getY());
				nP.setArrendatario(arrSesion.getArrendatario());
				nP.setImagenes(newPredio.getImagenes());
				preSesion.setPredio(nP);
				preBussiness.save(nP);
			System.out.println("Registro guardado");
			return "create-imagen?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String eliminar(String codigo) {
		
		try {
			preBussiness.eliminar(codigo);
			List<Imagen> aux = imaDao.getImagenesxPredio(codigo);
			for (int i = 0; i < aux.size(); i++) {
				Path myfile = Paths.get(aux.get(i).getDireccion());
				Files.delete(myfile);
			}
			System.out.println("Registro eliminado");
			return "list-predios-pruebas?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al eliminar");
			e.printStackTrace();			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String editar(Predio predio) {
		editing = true;
		System.out.println(predio);
		//newPersona = persona;
		return "create-predio?faces-redirect=true&id="+predio.getCodigo();
	}
	
	public String mostrarPredio(Predio predio) {
		
		editing = true;
		preSesionCliente.setPredio(predio);
		nPi5 = preBussiness.getArrendatario(preSesionCliente.getPredio().getCodigo());
		arrSesCli.setArrendatario(nPi5.get(0).getArrendatario());
		System.out.println(arrSesCli.getArrendatario().getEmail()+"  aaaaaaaaaaaaaaaaaaaaa bbbbbbbbbbbbbbbbbbb cccccccccccccccccccc");
		preImaSesCli.setCodigo(preSesionCliente.getPredio().getCodigo());
		//preImaSesion.setCodigo(preSesionCliente.getPredio().getCodigo());
		System.out.println(predio);
		//newPersona = persona;
		Interes interes = new Interes();
		int aux = Integer.parseInt(preSesionCliente.getPredio().getCodigo());
		System.out.println(aux+1);
		interes.setValor(aux);
		intDao.insert(interes);
		/*intBean.setnewInteres(interes);
		intBean.guardar();*/
		return "view-predio?faces-redirect=true&id="+predio.getCodigo();
	}
	
	
	public String obtenerCodigo(String codigo_predio) {
		preImaSesion.setCodigo(codigo_predio);
		/*if (nPi.size() > 0) {
			for (int i = 0; i < nPi.size(); i++) {
				for (int j = 0; j < nPi.get(i).getImagenes().size(); j++) {
					System.out.println(nPi.get(i).getCodigo()+" aaaaaaaaaaaaaaaaaaaaaaa "+nPi.get(i).getImagenes().get(j).getDireccion());
				}	
			}
			
		} else {
			return null;
		}	*/
		return "list-imagenes?faces-redirect=true";
	}
	
	/*public void contador(String b) {
		System.out.println(b+" bbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		int a = Integer.parseInt(b);
		Double c = intDao.getContarVistosPredios(1);
		System.out.println(c +" aaaaaaaaaaaaaaaaaaaaaaaaaaaa");
	}*/
	
	public void mandarCorreo() {
		  // El correo gmail de envío
		  String correoEnvia = "jpelaezt92@gmail.com";
		  String claveCorreo = "jybeyoxvwiixkpax";
		  
		  // La configuración para enviar correo
		  Properties properties = new Properties();
		 
		  properties.put("mail.smtp.host", "smtp.gmail.com");
		  properties.put("mail.smtp.port", "587");

		  properties.put("mail.smtp.starttls.enable", "true");
		  properties.put("mail.smtp.auth", "true");
		  properties.put("mail.user", correoEnvia);
		  properties.put("mail.password", claveCorreo);
		 
		  // Obtener la sesion
		  Session session = Session.getInstance(properties, null);
		  int aviso = 0;
		  try {
		   // Crear el cuerpo del mensaje
		   MimeMessage mimeMessage = new MimeMessage(session);
		 
		   // Agregar quien envía el correo
		   mimeMessage.setFrom(new InternetAddress(correoEnvia, "!ArriendaYa!"));
		    
		   // Los destinatarios
		   String correo = arrSesCli.getArrendatario().getEmail();
		   System.out.println(correo);
		   InternetAddress[] internetAddresses = {new InternetAddress(correo)};
//		     new InternetAddress("correo2@gmail.com"),
//		     new InternetAddress("correo3@gmail.com") };
		 
		   // Agregar los destinatarios al mensaje
		   mimeMessage.setRecipients(Message.RecipientType.TO,
		     internetAddresses);
		 
		   // Agregar el asunto al correo
		   mimeMessage.setSubject("Alguien quiere arrendar tu predio");
		 
		   // Creo la parte del mensaje
		   MimeBodyPart mimeBodyPart = new MimeBodyPart();
		   mimeBodyPart.setText("El usuario "+cliSesion.getCliente().getNombre()+" "+cliSesion.getCliente().getApellido()+" esta interesado en el predio con codigo "+preSesionCliente.getPredio().getCodigo()+" ubicado en la direccion "+preSesionCliente.getPredio().getDireccion()+", puedes contactarte con el mediante el E-mail "+cliSesion.getCliente().getEmail()+".");
		 
		   //MimeBodyPart mimeBodyPartAdjunto = new MimeBodyPart();
		   //mimeBodyPartAdjunto.attachFile("C:/Users/Public/Pictures/Sample Pictures/Penguins.jpg");

			// Crear el multipart para agregar la parte del mensaje anterior
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);
			//multipart.addBodyPart(mimeBodyPartAdjunto);
		   
		   // Agregar el multipart al cuerpo del mensaje
		   mimeMessage.setContent(multipart);
		 
		   // Enviar el mensaje
		   Transport transport = session.getTransport("smtp");
		   transport.connect(correoEnvia, claveCorreo);
		   transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
		   
		   transport.close();
		 
		  } catch (Exception ex) {
		   ex.printStackTrace();
		   System.out.println("Error: "+ex.getMessage());
		   //JOptionPane.showMessageDialog(null, "Error: "+ex.getMessage());
		   aviso = 1;
		  }
		  if (aviso==0) {
			  //JOptionPane.showMessageDialog(null, "¡Correo electrónico enviado exitosamente!");
			  System.out.println("¡Correo electrónico enviado exitosamente!");
		  }
		 }
	
	public String regresar() {
		return "loginCliente";
	}
}
