package ec.edu.ups.appdis.view;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import ec.edu.ups.appdis.bussiness.ClienteBussiness;
import ec.edu.ups.appdis.bussiness.ClienteSesion;
import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.modelo.Cliente;

@ManagedBean
@ViewScoped
public class ClienteBean {
	
	@Inject
	private ClienteBussiness cliBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private ClienteDAO cliDao;
	
	@Inject
	private ClienteSesion cliSesion;
	
	private Cliente newCliente;
	
	private String id;  		//Parametro para edicion
	
	private List<Cliente> clientes;
	
	private List<Cliente> clientesYpredios;
	
	private boolean editing;
	
	@PostConstruct
	public void init() {
		newCliente = new Cliente();
		//newCliente.addTelefono(new Telefono());
		editing = false;
		clientes = cliBussiness.getListadoCliente();
	}	

	
	public void loadData() {
		System.out.println("load data " + id);
		if(id==null)
			return;
		try {
			newCliente = cliBussiness.getCliente(id);
			editing = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}
	}
	public Cliente getnewCliente() {
		return newCliente;
	}
	public void setnewCliente(Cliente newCliente) {
		this.newCliente = newCliente;
	}
	public List<Cliente> getClientes() {
		return clientes;
	}
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		System.out.println("id param " + id);
		this.id = id;
	}


	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public ClienteSesion getcliSesion() {
		return cliSesion;
	}


	public void setcliSesion(ClienteSesion cliSesion) {
		this.cliSesion = cliSesion;
	}

	public List<Cliente> getClientesYpredios() {
		return clientesYpredios;
	}

	public void setClientesYpredios(List<Cliente> clientesYpredios) {
		this.clientesYpredios = clientesYpredios;
	}

	public String guardar() {
		System.out.println("editando " + editing);
		try {
			if(editing)
				cliBussiness.actualizar(newCliente);
			else
				cliBussiness.save(newCliente);
			System.out.println("Registro guardado");
			return "loginCliente?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String eliminar(String cedula) {
		
		try {
			cliBussiness.eliminar(cedula);
			System.out.println("Registro eliminado");
			return "list-clientes?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al eliminar");
			e.printStackTrace();			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String editar(Cliente cliente) {
		editing = true;
		System.out.println(cliente);
		//newPersona = persona;
		return "edit-cliente?faces-redirect=true&id="+cliente.getCedula();
	}
	
	public String login() {
		newCliente = cliDao.loginCliente(newCliente.getUsuario(), newCliente.getClave());
		if (newCliente == null) {
			newCliente = new Cliente();
			FacesContext.getCurrentInstance().addMessage( null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuario o contrasena incorrectos!", " Login Error!")); 
			return null;
		} else {
			Cliente cliente = new Cliente();
			cliSesion.setUsuario(newCliente.getUsuario());
			cliSesion.setEmail(newCliente.getEmail());
			cliSesion.setCedula(newCliente.getCedula());
			cliente.setCedula(newCliente.getCedula());
			cliente.setNombre(newCliente.getNombre());
			cliente.setApellido(newCliente.getApellido());
			cliente.setEmail(newCliente.getEmail());
			cliente.setUsuario(newCliente.getUsuario());
			cliente.setClave(newCliente.getClave());
			cliSesion.setCliente(cliente);
			return "list-predios?faces-redirect=true";
		}
	}
	
	public void logout() {
		ExternalContext ec=FacesContext.getCurrentInstance().getExternalContext();
		ec.invalidateSession();
		//return "/home.xhtml?faces-redirect=true";\
        try {
			ec.redirect(ec.getRequestContextPath() + "/loginCliente.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<Cliente> cliente(String usuario) {
		List<Cliente> a = cliDao.getClienteUnico2(usuario);
		return a;
	}
	
	public void validadorCedula(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String cedula = (String)arg2;
		String aux = cliDao.cedula(cedula);
		System.out.println(aux);
		if (aux.equals("no")) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("Cedula invalida"));
		}
	}
	
	public void validadorUsuario(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String usuario = (String)arg2;
		Cliente aux = cliDao.getUsuario(usuario); 
		System.out.println(aux);
		if (aux != null) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("El usuario ya existe"));
		}
	}
	
	public void validadorCorreo(FacesContext arg0, UIComponent arg1, Object arg2) {
		arg0 = FacesContext.getCurrentInstance();
		String correo = (String)arg2;
		Cliente aux = cliDao.getCorreo(correo); 
		System.out.println(aux);
		if (aux != null) {
			((UIInput)arg1).setValid(false);
			arg0.addMessage(arg1.getClientId(arg0), new FacesMessage("El correo ya esta en uso"));
		}
	}
	
	public String regresar() {
		return "loginCliente";
	}
	
	public void salir() {
		ExternalContext ec=FacesContext.getCurrentInstance().getExternalContext();
		ec.invalidateSession();
		//return "/home.xhtml?faces-redirect=true";\
        try {
			ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
