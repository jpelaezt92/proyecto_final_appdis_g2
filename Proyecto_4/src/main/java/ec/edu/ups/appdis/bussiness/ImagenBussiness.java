package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.ImagenDAO;
import ec.edu.ups.appdis.modelo.Imagen;


@Stateless
public class ImagenBussiness {

	@Inject
	private ImagenDAO imaDAO;
	
	public boolean save(Imagen imagen) throws Exception {
		Imagen aux = imaDAO.read(imagen.getCodigo());
		if(aux!=null)
			throw new Exception("Imagen ya existe");
		else
			imaDAO.insert(imagen);	
		
		return true;
	}
	
	public List<Imagen> getListadoImagen(){
		return imaDAO.getImagens();
	}
	
	public void eliminar(String codigo) throws Exception {
		Imagen aux = imaDAO.read(codigo);
		if(aux==null)
			throw new Exception("Imagen no existe");
		else
			imaDAO.remove(codigo);
	}
	
	public Imagen getImagen(String codigo) throws Exception {
		Imagen aux = imaDAO.read(codigo);
		if(aux==null)
			throw new Exception("Imagen no existe");
		else
			return aux;
	}
	
	public void actualizar(Imagen imagen) throws Exception {
		Imagen aux = imaDAO.read(imagen.getCodigo());
		if(aux==null)
			throw new Exception("Imagen no existe");
		else
			imaDAO.update(imagen);
	}
	
	public String guardar(Imagen imagen) throws Exception {
				
			imaDAO.insert(imagen);	
		
		return "registrado";
	}
}
