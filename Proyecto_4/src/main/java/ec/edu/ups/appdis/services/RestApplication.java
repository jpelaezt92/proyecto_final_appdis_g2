package ec.edu.ups.appdis.services;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/srv")
public class RestApplication extends Application {

}
