package ec.edu.ups.appdis.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.Part;

import ec.edu.ups.appdis.modelo.Imagen;
import ec.edu.ups.appdis.modelo.Predio;


@ManagedBean
public class Fileupload {
	
	private Part uploadedFile;
	private String folder = "c:\\files";
	private String h;
	private Predio pre;

	
	public String getH() {
		return h;
	}

	public void setH(String h) {
		this.h = h;
	}

	public Part getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(Part uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	
	public String saveFile(){
		
		try (InputStream input = uploadedFile.getInputStream()) {
			int a =  uploadedFile.getSubmittedFileName().length();
			//System.out.println(a);
			ArrayList<String> n = new ArrayList<String>();
			String s = "\\";
			String b = uploadedFile.getSubmittedFileName();
			//System.out.println(b);
			String[] str=b.replaceAll(Pattern.quote(s), "\\\\").split("\\\\");
			for (int i = 0; i < str.length; i++) {
				n.add(str[i]);
			}
			
			//System.out.println(str[0]+" "+a+" "+str[1]+" "+n.size()+" "+n.get(n.size()-1)+"1");
			String fileName = n.get(n.size()-1);
			//System.out.println(fileName);
	        Files.copy(input, new File(folder, fileName).toPath());
	        String f = folder+"\\"+ fileName;
	        System.out.println(f);



	        return f;
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	        return null;
	    }
	}
public String readFile(){
		String asd = null;
		try (InputStream input = uploadedFile.getInputStream()) {
			int a =  uploadedFile.getSubmittedFileName().length();
			ArrayList<String> n = new ArrayList<String>();
			String s = "\\";
			String b = uploadedFile.getSubmittedFileName();
			String[] str=b.replaceAll(Pattern.quote(s), "\\\\").split("\\\\");
			for (int i = 0; i < str.length; i++) {
				n.add(str[i]);
			}
			
			System.out.println(str[0]+" "+a+" "+str[1]+" "+n.size()+" "+n.get(n.size()-1)+"1");
			String fileName = n.get(n.size()-1);
			asd =folder+"\\"+fileName;
			//h = asd + "1";
	        Files.copy(input, new File(folder, fileName).toPath());
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	    }
		
		return asd;
	}

}
