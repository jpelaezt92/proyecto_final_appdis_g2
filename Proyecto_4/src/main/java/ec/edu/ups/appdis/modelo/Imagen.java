package ec.edu.ups.appdis.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_imagen_prueba")
public class Imagen {
	
	@Id
	@Column(name = "ima_codigo")
	private String codigo;
	
	@Column(name = "ima_nombre")
	private String nombre;
	
	@Column(name = "ima_direccion")
	private String direccion;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ima_pre_codigo")
	private Predio predio;
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Predio getPredio() {
		return predio;
	}

	public void setPredio(Predio predio) {
		this.predio = predio;
	}
}
