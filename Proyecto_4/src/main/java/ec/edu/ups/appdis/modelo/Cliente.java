package ec.edu.ups.appdis.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name="tbl_cliente_prueba")
public class Cliente {
	
	@Id
	@Column(name = "cli_cedula")
	@NotNull(message = "No puede estar vacio")
	@Size.List({
		@Size(min = 10, message = "La cedula ingresada debe tener mas de diez digitos"),
		@Size(max = 10, message = "La cedula ingresada debe tener menos de diez digitos")
	})
	private String cedula;

	@Column(name = "cli_nombre")
	@NotNull(message = "No puede estar vacio")
	@Size.List({
		@Size(min = 4, message = "El nombre ingresado debe tener mas de cuatro caracteres"),
		@Size(max = 20, message = "El nombre ingresado debe tener menos de veinte caracteres")
	})
	private String nombre;
	
	@Column(name = "cli_apellido")
	@NotNull(message = "No puede estar vacio")
	@Size.List({
		@Size(min = 4, message = "El apellido ingresado debe tener mas de cuatro caracteres"),
		@Size(max = 20, message = "El apellido ingresado debe tener menos de veinte caracteres")
	})
	private String apellido;
	
	@Column(name = "cli_email")
	@NotNull(message = "No puede estar vacio")
	@Email(message = "El correo ingresado no es valido")
	private String email;
	
	@Column(name = "cli_usuario")
	@NotNull(message = "No puede estar vacio")
	@Size.List({
		@Size(min = 4, message = "El usuario ingresado debe tener mas de cuatro caracteres"),
		@Size(max = 11, message = "El usuario ingresado debe tener menos de once caracteres")
	})
	private String usuario;
	
	@Column(name = "cli_clave")
	@NotNull(message = "No puede estar vacio")
	@Size.List({
		@Size(min = 4, message = "La clave ingresada debe tener mas de cuatro caracteres"),
		@Size(max = 10, message = "La clave ingresada debe tener menos de diez caracteres")
	})
	private String clave;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
}