package ec.edu.ups.appdis.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import ec.edu.ups.appdis.modelo.Cliente;

@Stateless
public class ClienteDAO {
	
	@Inject
	private EntityManager em;

	public void insert(Cliente cliente) {
		em.persist(cliente);
	}
	
	public void update(Cliente cliente) {
		em.merge(cliente);
	}
	
	public void remove(String id) {
		em.remove(this.read(id));
	}
	
	public Cliente read(String id) {
		Cliente cliente = em.find(Cliente.class, id);
		return cliente;
	}
	
	public List<Cliente> getClientes(){
		String jpql = "SELECT cli FROM Cliente cli";
		Query query = em.createQuery(jpql, Cliente.class);
		List<Cliente> listado = query.getResultList();		
		return listado;
	}
	
	public Cliente loginCliente(String usuario, String clave) {
		try {
			String jpql = "SELECT cli FROM Cliente cli WHERE cli.usuario = :variable1 and cli.clave = :variable2";
			Cliente query = (Cliente) em.createQuery(jpql).setParameter("variable1", usuario) .setParameter("variable2", clave).getSingleResult();
			return query;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}
	
	public List<Cliente> getClienteUnico2(String usuario) {
		String jpql = "SELECT cli FROM Cliente cli WHERE cli.usuario = :variable1";
		Query query = em.createQuery(jpql, Cliente.class).setParameter("variable1", usuario);
		List<Cliente> lista = query.getResultList();
		return lista;
	}
	
	public String cedula(String cedula) {
		String hola = "no";
		try {
			ArrayList<String> cedula1 = new ArrayList<String>();
			ArrayList<Integer> cedula2 = new ArrayList<Integer>();
			for (int i = 1; i <= cedula.length(); i++) {
				cedula1.add(cedula.substring(i-1, i));
				//System.out.println(cedula1.get(i-1));
				cedula2.add(Integer.parseInt(cedula1.get(i-1)));
				//System.out.println(cedula2.get(i-1));
			}
			ArrayList<Integer> cedula3 = new ArrayList<Integer>();
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			cedula3.add(1);
			cedula3.add(2);
			ArrayList<Integer> cedula4 = new ArrayList<Integer>();
			ArrayList<Integer> cedula5 = new ArrayList<Integer>();
			for (int i = 0; i <= cedula3.size()-1; i++) {
				cedula4.add(cedula2.get(i)*cedula3.get(i));
				if (cedula4.get(i)>10) {
					int decena = cedula4.get(i)/10;
					int unidad = cedula4.get(i)%10;
					cedula5.add(decena+unidad);
					//System.out.println(cedula5.get(i));
				} else {
					cedula5.add(cedula4.get(i));
					//System.out.println(cedula5.get(i));
				}
			}
			int subtotal = cedula5.get(0)+cedula5.get(1)+cedula5.get(2)+cedula5.get(3)+cedula5.get(4)+cedula5.get(5)+cedula5.get(6)+cedula5.get(7)+cedula5.get(8);
			//System.out.println(subtotal);
			int aux = ((subtotal/10)+1)*10;
			//System.out.println(aux);
			int total = aux-subtotal;
			//System.out.println(total);
			int aux2 = subtotal%10;
			//System.out.println(aux2);
			if (aux2==cedula2.get(9)) {
				hola = "si";
			} else if (total==cedula2.get(9)) {
				hola = "si";
			} 
			

		} catch (Exception e) {
			// TODO: handle exception
			
		}
		return hola;
	}
	
	public Cliente getUsuario(String usuario){ 
		try {
			Cliente cliente = (Cliente) em .createQuery( "SELECT cli FROM Cliente cli WHERE cli.usuario = :variable1").setParameter("variable1", usuario).getSingleResult();
			System.out.println(cliente);
			return cliente; 
			} catch (NoResultException e) { 
				
				}
		return null; 
	}
	
	public Cliente getCorreo(String correo){ 
		try {
			Cliente cliente = (Cliente) em .createQuery( "SELECT cli FROM Cliente cli WHERE cli.email = :variable1").setParameter("variable1", correo).getSingleResult();
			System.out.println(cliente);
			return cliente; 
			} catch (NoResultException e) { 
				return null; 
				}
	}
	
}
