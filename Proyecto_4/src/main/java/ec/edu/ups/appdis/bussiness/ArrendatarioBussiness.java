package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.ArrendatarioDAO;
import ec.edu.ups.appdis.modelo.Arrendatario;

@Stateless
public class ArrendatarioBussiness {

	@Inject
	private ArrendatarioDAO arrDAO;
	
	
	public boolean save(Arrendatario arrendatario) throws Exception {
		Arrendatario aux = arrDAO.read(arrendatario.getCedula());
		if(aux!=null)
			throw new Exception("Arrendatario ya existe");
		else
			arrDAO.insert(arrendatario);	
		
		return true;
	}
	
	public List<Arrendatario> getListadoArrendatario(){
		return arrDAO.getArrendatarios();
	}
	
	public void eliminar(String cedula) throws Exception {
		Arrendatario aux = arrDAO.read(cedula);
		if(aux==null)
			throw new Exception("Arrendatario no existe");
		else
			arrDAO.remove(cedula);
	}
	
	public Arrendatario getArrendatario(String cedula) throws Exception {
		Arrendatario aux = arrDAO.read(cedula);
		if(aux==null)
			throw new Exception("Arrendatario no existe");
		else
			return aux;
	}
	
	public void actualizar(Arrendatario arrendatario) throws Exception {
		Arrendatario aux = arrDAO.read(arrendatario.getCedula());
		if(aux==null)
			throw new Exception("Arrendatario no existe");
		else
			arrDAO.update(arrendatario);
	}
	
	public String guardar(Arrendatario arrendatario) throws Exception {
				
			arrDAO.insert(arrendatario);	
		
		return "registrado";
	}
}
