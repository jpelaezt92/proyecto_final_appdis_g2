package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.PredioDAO;
import ec.edu.ups.appdis.modelo.Predio;

@Stateless
public class PredioBussiness {

	@Inject
	private PredioDAO preDAO;
	
	
	public boolean save(Predio predio) throws Exception {
		Predio aux = preDAO.read(predio.getCodigo());
		if(aux!=null)
			throw new Exception("predio ya existe");
		else
			preDAO.insert(predio);	
		
		return true;
	}
	
	public List<Predio> getListadoPredio(){
		return preDAO.getPredios();
	}
	
	public void eliminar(String codigo) throws Exception {
		Predio aux = preDAO.read(codigo);
		if(aux==null)
			throw new Exception("Predio no existe");
		else
			preDAO.remove(codigo);
	}
	
	public Predio getPredio(String codigo) throws Exception {
		Predio aux = preDAO.read(codigo);
		if(aux==null)
			throw new Exception("Predio no existe");
		else
			return aux;
	}
	
	public void actualizar(Predio predio) throws Exception {
		Predio aux = preDAO.read(predio.getCodigo());
		if(aux==null)
			throw new Exception("Predio no existe");
		else
			preDAO.update(predio);
	}
	
	public String guardar(Predio predio) throws Exception {
				
			preDAO.insert(predio);	
		
		return "registrado";
	}
	
	public List<Predio> getListadoPredioYarrendatario(String cedula){
		return preDAO.getArrendatarioYpredio(cedula);
	}
	
	public void MostrarPredio(Predio predio) throws Exception {
		Predio aux = preDAO.read(predio.getCodigo());
		if(aux==null)
			throw new Exception("Predio no existe");
		else
			preDAO.update(predio);
	}
	
	public List<Predio> getListadoPredioYimagen(String codigo){
		return preDAO.getImagenesYpredio(codigo);
	}
	
	public List<Predio> getArrendatario(String codigo){
		return preDAO.getArrendatario(codigo);
	}
}
