package ec.edu.ups.appdis.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import ec.edu.ups.appdis.bussiness.InteresBussiness;
import ec.edu.ups.appdis.bussiness.PredioSesionCliente;
import ec.edu.ups.appdis.modelo.Interes;
import ec.edu.ups.appdis.modelo.Predio;

@ManagedBean
@ViewScoped
public class InteresBean {
	
	@Inject
	private InteresBussiness intBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private PredioSesionCliente preSesionCliente;
	
	private Interes newInteres;	
	
	private List<Interes> intereses;
	
	private int aux1;
	
	private int aux2;
	
	private String contador;
	
	@PostConstruct
	public void init() {
		newInteres = new Interes();
		//newPredio.addImagen(new Imagen());
		 aux1 = Integer.parseInt(preSesionCliente.getPredio().getCodigo());
		intereses = intBussiness.getListadoInteresxPredio(aux1);
		aux2 = intereses.size();
		if (aux2 <= 1) {
			contador = "! "+aux2+"  persona ha visto este predio contactate ya!";
		} else {
			contador = "! "+aux2+"  personas han visto este predio contactate ya!";
		}	
	}	
	
	public Interes getnewInteres() {
		return newInteres;
	}
	public void setnewInteres(Interes newInteres) {
		this.newInteres = newInteres;
	}

	public List<Interes> getIntereses() {
		return intereses;
	}

	public void setIntereses(List<Interes> intereses) {
		this.intereses = intereses;
	}

	public int getAux2() {
		return aux2;
	}

	public void setAux2(int aux2) {
		this.aux2 = aux2;
	}

	public String getContador() {
		return contador;
	}

	public void setContador(String contador) {
		this.contador = contador;
	}

	public String guardar() {
		try {
			
			intBussiness.save(newInteres);
			System.out.println("Registro guardado");
			return "create-imagen?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	

	
	
}
