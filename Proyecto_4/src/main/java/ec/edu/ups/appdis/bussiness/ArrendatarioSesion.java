package ec.edu.ups.appdis.bussiness;


import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ec.edu.ups.appdis.modelo.Arrendatario;

@Named 
@SessionScoped
public class ArrendatarioSesion implements Serializable{
	
	private String cedula;
	private String nombre;
	private String apellido;
	private String email;
	private String usuario;
	private String clave;
	
	private Arrendatario arrendatario;
	
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getClave() {
		return clave;
	}
	
	public void setClave(String clave) {
		this.clave = clave;
	}
	public Arrendatario getArrendatario() {
		return arrendatario;
	}
	public void setArrendatario(Arrendatario arrendatario) {
		this.arrendatario = arrendatario;
	}

}
