package ec.edu.ups.appdis.services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ec.edu.ups.appdis.bussiness.PredioSesionServicio;
import ec.edu.ups.appdis.dao.ClienteDAO;
import ec.edu.ups.appdis.dao.ImagenDAO;
import ec.edu.ups.appdis.dao.PredioDAO;
import ec.edu.ups.appdis.modelo.Cliente;
import ec.edu.ups.appdis.modelo.Imagen;
import ec.edu.ups.appdis.modelo.Predio;

@Path("/clientes")
public class ClienteService {
	
	@Inject
	private ClienteDAO cliDAO;
	
	@Inject
	private PredioDAO preDAO;
	
	@Inject
	private ImagenDAO imaDAO;
	
	@Inject
	private PredioSesionServicio preSesionSer;
	
	@GET
	@Path("/login")
	@Produces("application/json")
	public ArrayList<String> getLoginCliente(@QueryParam("usuario") String usuario, @QueryParam("clave") String clave){
		Cliente aux = cliDAO.loginCliente(usuario, clave);
		ArrayList<String> newCliente = new ArrayList<>();
		newCliente.add(aux.getCedula());
		newCliente.add(aux.getNombre());
		newCliente.add(aux.getApellido());
		newCliente.add(aux.getEmail());
		newCliente.add(aux.getUsuario());
		newCliente.add(aux.getClave());
		return newCliente;
	}
	
	@POST
	@Path("/crearCliente")
	@Produces("application/json")
	@Consumes("application/json")
	public void getCrearCliente(ArrayList<String> newCliente){
		try {
			Cliente cliente = new Cliente();
			cliente.setCedula(newCliente.get(0));
			cliente.setNombre(newCliente.get(1));
			cliente.setApellido(newCliente.get(2));
			cliente.setEmail(newCliente.get(3));
			cliente.setUsuario(newCliente.get(4));
			cliente.setClave(newCliente.get(5));
			cliDAO.insert(cliente);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@GET
	@Path("/listadoPredios")
	@Produces("application/json")
	public List<Predio> predios(){
		return preDAO.getPredios();
	}
	
	@GET
	@Path("/listadoImagenesxPredio")
	@Produces("application/json")
	public List<Imagen> ImagenesxPredio(@QueryParam("codigo") String codigo){
		return imaDAO.getImagenesxPredio(codigo);
	}
	
	@GET
	@Path("/verUbicacionxPredio")
	@Produces("application/json")
	public List<Predio> ubicacion(@QueryParam("codigo") String codigo){
		return preDAO.getArrendatario(codigo);
	}
	
	
	

}
