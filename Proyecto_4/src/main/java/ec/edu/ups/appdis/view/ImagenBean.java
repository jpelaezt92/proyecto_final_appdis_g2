package ec.edu.ups.appdis.view;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.Part;

import ec.edu.ups.appdis.bussiness.ImagenBussiness;
import ec.edu.ups.appdis.bussiness.PredioSesion;
import ec.edu.ups.appdis.dao.ImagenDAO;
import ec.edu.ups.appdis.modelo.Imagen;
import ec.edu.ups.appdis.modelo.Predio;

@ManagedBean
@ViewScoped
public class ImagenBean {
	
	@Inject
	private ImagenBussiness imaBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	@Inject
	private PredioSesion preSesion;
	
	@Inject
	private ImagenDAO imaDAO;
	
	private Imagen newImagen;
	
	private String id;  		//Parametro para edicion
	
	private List<Imagen> imagenes;
	
	private List<String> imagenes1;
	
	private boolean editing;
	
	private Imagen nI;
	
	private Part uploadedFile;
	
	private String folder = "A:\\eclipse-workspace\\Proyecto_4\\src\\main\\webapp\\resources\\img\\photo";
	//private String folder = "Proyecto_4/src/main/webapp/resources/img/photo/";
	
	@PostConstruct
	public void init() {
		newImagen = new Imagen();
		editing = false;
		imagenes = imaBussiness.getListadoImagen();
	}	

	
	public void loadData() {
		System.out.println("load data " + id);
		if(id==null)
			return;
		try {
			newImagen = imaBussiness.getImagen(id);
			editing = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}
	}
	public Imagen getNewImagen() {
		return newImagen;
	}
	
	public void setNewImagen(Imagen newImagen) {
		this.newImagen = newImagen;
	}
	
	public List<Imagen> getImagenes() {
		return imagenes;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		System.out.println("id param " + id);
		this.id = id;
	}

	public boolean isEditing() {
		return editing;
	}

	public void setEditing(boolean editing) {
		this.editing = editing;
	}

	public void setImagenes(List<Imagen> imagenes) {
		this.imagenes = imagenes;
	}

	public Part getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(Part uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public List<String> getImagenes1() {
		return imagenes1;
	}


	public void setImagenes1(List<String> imagenes1) {
		this.imagenes1 = imagenes1;
	}


	public String guardar() {
		System.out.println("editando " + editing);
		int aux = imagenes.size();
		String aux3;
		if (aux == 0) {
			aux3 = "1";
		} else {
			String aux1 = imagenes.get(aux-1).getCodigo();
			int aux2 = Integer.parseInt(aux1)+1;
			aux3 = String.valueOf(aux2);
		}
		try {
			if(editing)
				imaBussiness.actualizar(newImagen);
			else
				try (InputStream input = uploadedFile.getInputStream()) {
					int a =  uploadedFile.getSubmittedFileName().length();
					//System.out.println(a);
					ArrayList<String> n = new ArrayList<String>();
					String s = "\\";
					String b = uploadedFile.getSubmittedFileName();
					//System.out.println(b);
					String[] str=b.replaceAll(Pattern.quote(s), "\\\\").split("\\\\");
					for (int i = 0; i < str.length; i++) {
						n.add(str[i]);
					}
					//System.out.println(str[0]+" "+a+" "+str[1]+" "+n.size()+" "+n.get(n.size()-1)+"1");
					String fileName = n.get(n.size()-1);
					//System.out.println(fileName);
			        Files.copy(input, new File(folder, fileName).toPath());
			        //String f = folder+"\\"+ fileName;
			        String f = "resources/img/photo/"+ fileName;
			        System.out.println(f);
			        nI = new Imagen();
					nI.setCodigo(aux3);
					nI.setNombre(fileName);
					nI.setDireccion(f);
					nI.setPredio(preSesion.getPredio());
					imaBussiness.save(nI);
					nI = new Imagen();
				System.out.println("Registro guardado");
				return "create-imagen?faces-redirect=true";
			    }
			    catch (IOException e) {
			        e.printStackTrace();
			        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
			        		e.getMessage(), "Error");
		            facesContext.addMessage(null, m);
			        return null;
			    }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String guardar2() {
		System.out.println("editando " + editing);
		int aux = imagenes.size();
		String aux3;
		if (aux == 0) {
			aux3 = "1";
		} else {
			String aux1 = imagenes.get(aux-1).getCodigo();
			int aux2 = Integer.parseInt(aux1)+1;
			aux3 = String.valueOf(aux2);
		}
		try {
			if(editing)
				imaBussiness.actualizar(newImagen);
			else
				try (InputStream input = uploadedFile.getInputStream()) {
					int a =  uploadedFile.getSubmittedFileName().length();
					//System.out.println(a);
					ArrayList<String> n = new ArrayList<String>();
					String s = "\\";
					String b = uploadedFile.getSubmittedFileName();
					//System.out.println(b);
					String[] str=b.replaceAll(Pattern.quote(s), "\\\\").split("\\\\");
					for (int i = 0; i < str.length; i++) {
						n.add(str[i]);
					}
					//System.out.println(str[0]+" "+a+" "+str[1]+" "+n.size()+" "+n.get(n.size()-1)+"1");
					String fileName = n.get(n.size()-1);
					//System.out.println(fileName);
			        Files.copy(input, new File(folder, fileName).toPath());
			        String f = "resources/img/photo/"+ fileName;
			        System.out.println(f);
			        nI = new Imagen();
					nI.setCodigo(aux3);
					nI.setNombre(fileName);
					nI.setDireccion(f);
					nI.setPredio(preSesion.getPredio());
					imaBussiness.save(nI);
					nI = new Imagen();
				System.out.println("Registro guardado");
				return "arrendatario-principal?faces-redirect=true";
			    }
			    catch (IOException e) {
			        e.printStackTrace();
			        return null;
			    }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			e.printStackTrace();
			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}	
	
	public String eliminar(String codigo) {
		
		try {
			imaBussiness.eliminar(codigo);
			System.out.println("Registro eliminado");
			return "list-imagenes?faces-redirect=true";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al eliminar");
			e.printStackTrace();			
			
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, 
					e.getMessage(), "Error");
            facesContext.addMessage(null, m);
		}		
		return null;
	}
	
	public String editar(Imagen imagen) {
		editing = true;
		System.out.println(imagen);
		//newPersona = persona;
		return "create-imagen?faces-redirect=true&id="+imagen.getCodigo();
	}
	
	public String editar2(Imagen imagen) {
		editing = true;
		System.out.println(imagen);
		//newPersona = persona;
		return "create-imagen?faces-redirect=true&id="+imagen.getCodigo();
	}
	
	public List<Imagen> getImagenesxPredio(String codigo){
		return imaDAO.getImagenesxPredio(codigo);
	}
}
