package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.modelo.Arrendatario;
import ec.edu.ups.appdis.modelo.Predio;

@Stateless
public class PredioDAO {
	
	@Inject
	private EntityManager em;

	public void insert(Predio predio) {
		em.persist(predio);
	}
	
	public void update(Predio predio) {
		em.merge(predio);
	}
	
	public void remove(String id) {
		em.remove(this.read(id));
	}
	
	public Predio read(String id) {
		Predio predio = em.find(Predio.class, id);
		//predio.getImagenes().size();
		return predio;
	}
	
	public List<Predio> getPredios(){
		String jpql = "SELECT pre FROM Predio pre";
		Query query = em.createQuery(jpql, Predio.class);
		List<Predio> listado = query.getResultList();		
		return listado;
	}
	
	public List<Predio> getArrendatarioYpredio(String cedula) {
		String jpql = "SELECT pre FROM Predio pre JOIN FETCH pre.arrendatario WHERE pre.arrendatario.cedula = :variable1";
		Query query = em.createQuery(jpql, Predio.class).setParameter("variable1", cedula);
		List<Predio> lista = query.getResultList();
		return lista;
	}
	
	public List<Predio> getImagenesYpredio(String codigo) {
		String jpql = "SELECT pre FROM Predio pre JOIN FETCH pre.imagenes WHERE pre.codigo = :variable1";
		Query query = em.createQuery(jpql, Predio.class).setParameter("variable1", codigo);
		List<Predio> lista = query.getResultList();
		return lista;
	}
	
	public List<Predio> getArrendatario(String codigo) {
		String jpql = "SELECT pre FROM Predio pre JOIN FETCH pre.arrendatario WHERE pre.codigo = :variable1";
		Query query = em.createQuery(jpql, Predio.class).setParameter("variable1", codigo);
		List<Predio> lista = query.getResultList();
		return lista;
	}
}
